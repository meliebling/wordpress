package api;

import okhttp3.*;

import java.io.IOException;
import java.util.Map;

public class ApiClient {

    private static final String BASE_URL = "http://192.168.247.5:8080/wp-json/wp/v2";
    private static final OkHttpClient httpClient = new OkHttpClient();
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public static void sendGet() throws IOException {
        Request request = new Request.Builder()
                .url(BASE_URL + "/users")
                .addHeader("Authorization", Credentials.basic("user", "password"))
                .build();

        Call call = httpClient.newCall(request);
        Response response = call.execute();

        System.out.println(response.code());

        Headers responseHeaders = response.headers();
        for (int i = 0; i < responseHeaders.size(); i++) {
            System.out.println(responseHeaders.name(i) + ": " + responseHeaders.value(i));
        }

        System.out.println(response.body().string());
    }

    public static void sendPOST() throws IOException {

        RequestBody body = new FormBody.Builder()
                .add("username", "abc")
                .add("password", "password")
                .add("email", "abc@email.com")
                .build();

        Request request = new Request.Builder()
                .url(BASE_URL + "/users")
                .addHeader("User-Agent", "OkHttp Bot")
                .addHeader("Authorization", Credentials.basic("user", "password"))
                .post(body)
                .build();

        Response response = httpClient.newCall(request).execute();

        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        System.out.println(response.body().string());
    }

    public static void sendPOST(String jsonBody) throws IOException {

        RequestBody body = RequestBody.create(jsonBody, JSON);

        Request request = new Request.Builder()
                .url(BASE_URL + "/users")
                .addHeader("User-Agent", "OkHttp Bot")
                .addHeader("Authorization", Credentials.basic("user", "password"))
                .post(body)
                .build();

        Response response = httpClient.newCall(request).execute();

        if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

        System.out.println(response.body().string());
    }

    public static void main(String[] args) throws IOException {
//        sendPOST("{\"username\":\"user7\",\"first_name\":\"User\",\"last_name\":\"Seven\",\"email\":\"user7@mail.com\",\"url\":\"\",\"description\":\"\",\"roles\":[\"subscriber\"],\"password\":\"password\"}");
        sendGet();
    }
}